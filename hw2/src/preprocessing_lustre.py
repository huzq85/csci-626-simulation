import os
import pandas as pd

# h2ologin2 -> 15393239
# h2ologin3 -> 15503736
# h2ologin4 -> 13854682
# h2ologin1 -> 15343484

# HOST_KEY = 'h2ologin3'
HEADER = ['host_name', 'time', 'operation',
          'filesystem', 'ost_ID', 'measurement_time']


def get_host_names(total_lines):
    hosts = {}
    for line in total_lines:
        host_name = line.split(',')[0]
        if host_name not in hosts.keys():
            hosts[host_name] = 1
        else:
            hosts[host_name] += 1

    return hosts


def read_files(f_dir):
    total_lines = []
    for root, dirs, files in os.walk(f_dir):
        for file_name in files:
            print(f'Current reading file: {file_name}')
            file_path = os.path.join(f_dir, file_name)
            with open(file_path) as f:
                lines = f.readlines()
                for line in lines:
                    if ':' in line:
                        # Skipping invalid lines
                        continue
                    total_lines.append(line.strip())
    return total_lines


def get_data(f_dir):
    elems_matrix = []

    total_lines = read_files(f_dir)

    for line in total_lines:
        elems = line.split(',')[0:-1]
        elems = [elem.strip() for elem in elems]
        if len(elems) == 0:
            continue
        if elems[-1] != '':
            if len(elems) != len(HEADER):
                # Add this step because there might be two nodes data stored in the same line
                elems_matrix.append(elems[0:len(HEADER)])
            else:
                elems_matrix.append(elems)

    print(f'Total valid lines read: {len(elems_matrix)}')
    return elems_matrix


def save_data_frame(data_frame, file_name):
    data_frame.to_csv(file_name, index=False)


def load_data_frame(file_name):
    data_frame = pd.read_csv(file_name)
    return data_frame


def get_data_frame(f_dir):
    data = get_data(f_dir)
    data_frame = pd.DataFrame(data, columns=HEADER)
    return data_frame


def process_data_frame(whole_data_frame):
    # header = whole_data_frame.columns.values
    hosts = whole_data_frame['host_name'].unique()
    max_size = 0
    max_data_frame = pd.DataFrame()
    for host in hosts:
        df_per_host = whole_data_frame.loc[whole_data_frame['host_name'] == host]

        df_per_host = df_per_host.drop_duplicates('time', keep='last')
        if len(df_per_host) > max_size:
            max_size = len(df_per_host)
            max_data_frame = df_per_host

    print(f'Maximum Data Frame:\n{max_data_frame}')
    max_data_frame = max_data_frame.sort_values('time')
    return max_data_frame


def get_file_name(file_folder):
    # return a file name by year and month
    return os.listdir(file_folder)[0][:-2]


if '__main__' == __name__:
    # The following is the 1st part to read data and convert it to a data frame
    file_folder = r'/home/huzq85/courses/626-simulation/scratch'
    file_folder = r'/home/huzq85/courses/scratch'
    data_frame = get_data_frame(file_folder)

    # It's convenient for debugging purpose if saving the data frame.
    base_name = os.path.basename(file_folder)
    file_time = get_file_name(file_folder)
    save_data_frame(data_frame, f'saved_data_set_{base_name}_{file_time}.csv')

    #################################
    # The following is the 2nd part to load data frame from a saved dataset
    whole_data_frame = load_data_frame(
        f'saved_data_set_{base_name}_{file_time}.csv')
    max_data_frame = process_data_frame(whole_data_frame)
    save_data_frame(
        max_data_frame, 'max_data_frame_{base_name}_{file_time}.csv')
