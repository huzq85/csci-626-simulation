import numpy as np
from preprocessing import *

TIMESTAMP = '#Time'


def normalize(data):
    new_list = (data-np.min(data))/(np.max(data)-np.min(data))
    return list(new_list)


def compute_inter_arrival(headers, dataframe):
    dataframes_filtered_attr = filter_attributes(headers, dataframe)
    # We only choose one dataframe to do the computing in the following
    selected_dataframe = dataframes_filtered_attr[0]
    timestamps = selected_dataframe[TIMESTAMP]

    print(f'Timestamps:\n{timestamps}')
    print(type(timestamps))
    inter_arrival_times = []

    timestamps_list = timestamps.to_list()

    for curIndex in range(len(timestamps_list)-1):
        inter_arrival = float(
            timestamps_list[curIndex+1]) - float(timestamps_list[curIndex])
        inter_arrival_times.append(inter_arrival)

    # for inter_arrival in inter_arrivals:
    #     print(inter_arrival)

    normalized_inter_arrival_times = normalize(inter_arrival_times)
    mean, variance, std = compute_basic_statistics(
        normalized_inter_arrival_times)
    cv = std/mean * 100
    print('Statistics of Inter-arrival Times:')
    print(f'Mean: {mean:.6f}')
    print(f'variance: {variance:.6f}')
    print(f'Standard Deviation: {std:.6f}')
    print(f'C.V.: {cv:.6f}')
    return normalized_inter_arrival_times


def get_service_time(headers, dataframe):
    # Need to normalize the data
    dataframes_filtered_attr = filter_attributes(headers, dataframe)
    selected_dataframe = dataframes_filtered_attr[0]
    service_attrs = ATTRIBUTE_SERVICE_TIME[2:]
    service_attr_list = []
    for attr in service_attrs:
        if (attr == 'RDMA_rx_bytes'):
            print(f'Print SERVICE TIME: {attr}')
            service_attr = selected_dataframe[attr]
            service_attr_list = service_attr.to_list()
            service_attr_list = [float(x) for x in service_attr_list]
            service_attr_list = normalize(service_attr_list)
            mean, variance, std = compute_basic_statistics(service_attr_list)
            cv = std/mean * 100
            print('Statistics of Service Times:')
            print(f'Mean: {mean:.6f}')
            print(f'variance: {variance:.6f}')
            print(f'Standard Deviation: {std:.6f}')
            print(f'C.V.: {cv:.6f}')
    return service_attr_list


def compute_basic_statistics(list_vals):
    # Return the mean, variance and standard deviations
    return np.mean(list_vals), np.var(list_vals), np.std(list_vals)


def saving_compute_results(file_name, data):
    with open(file_name, 'w') as f:
        for elem in data:
            f.write(str(elem) + ' ')


if '__main__' == __name__:
    parent_file_path = r'/home/huzq85/courses/626-simulation/dataset'
    header_file = r'/home/huzq85/courses/626-simulation/hw2/sample_dataset/HEADER'
    # "20160815": 4.7 GB; "20170328":1.98 GB; "20180711"||"20180711-fake": Small size
    data_file = os.path.join(parent_file_path, '20160815')
    headers = get_header(header_file)
    data = get_data(data_file)
    inter_arrival_times = compute_inter_arrival(headers, data)
    service_times = get_service_time(headers, data)

    print(f'Inter-arrival Times:\n{inter_arrival_times}')
    print(f'Service Times:\n{service_times}')

    saving_compute_results('inter_arrival_times.txt', inter_arrival_times)
    saving_compute_results('service_times.txt', service_times)
