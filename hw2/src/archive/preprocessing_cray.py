import os
import pandas as pd


# NOTE: This script could only be executed on machines that have large size memories, such as bg1
KEY = 'CompId'
ATTRIBUTE_SERVICE_TIME = [
    '#Time',
    'CompId',
    'ipogif0_rx_bytes',
    'RDMA_rx_bytes',
    'RDMA_nrx',
    'RDMA_tx_bytes',
    'SMSG_rx_bytes',
    'direct_write#stats.snx11001',
    'write_bytes#stats.snx11001',
    'direct_write#stats.snx11002',
    'write_bytes#stats.snx11002',
    'direct_write#stats.snx11003',
    'write_bytes#stats.snx11003']
THRESHOLD = 5

FILE_MOST_OCCUR_NODES = 'MOST_OCCUR_NODES.csv'
FILE_FILTERED_ATTRIBUTES = 'FILTERED_ATTRIBUTES.csv'


def get_header(header_file):
    header_items = []
    with open(header_file) as f:
        line = f.readline()
        line = line.split(',')
        header_items = [item.strip() for item in line]
        # print(len(header_items))
        # print(header_items)
    return header_items


def get_data(data_file):
    elems_matrix = []

    with open(data_file) as f:
        lines = f.readlines()
        for line in lines:
            # print(type(line))
            elems_line = line.split(',')
            elems_line = [elem.strip() for elem in elems_line]
            # print(elems_line)
            elems_matrix.append(elems_line)
        print(len(elems_matrix))

        # for row in elems_matrix:
        #     print(row)
    return elems_matrix


def get_dataframe_full(headers, data):
    org_dataframe = pd.DataFrame(data, columns=[header for header in headers])
    # dataFrame = dataFrame.sort_values()
    print(f'DataFrame is\n:{org_dataframe}')
    return org_dataframe


def get_dataframe_CompId(headers, data):
    # One way is to find the nodes that have the most appearance
    # The other way is to group the nodes based on CompId
    org_dataframe = get_dataframe_full(headers, data)
    node_count = org_dataframe[KEY].value_counts()
    print(f'Type of nodeCount:{type(node_count)}')
    print(f'NodeID Counts:\n{node_count}')

    # # To group the nodes based on KEY (CompId) value
    # grouped_dataframes = []
    # nodes_group = node_count.index
    # for node in nodes_group:
    #     node_dataframe = org_dataframe.loc[(org_dataframe[KEY] == node)]
    #     grouped_dataframes.append(node_dataframe)

    # for df in grouped_dataframes:
    #     print(f'DataFrame:\n{df}')

    most_occurrent_number = node_count[0]
    # print(f'Most Occurrent Number: {most_occurrent_number}')
    most_occurrent_nodes = [
        node_id for node_id in node_count.index if node_count[node_id] == most_occurrent_number]

    # print(f'Most Occurrent Node:\n{most_occurrent_nodes}')
    # print(f'Nodes Number: {len(most_occurrent_nodes)}')

    most_occurrent_nodes_dataframe = []
    collected_num = 0
    for node in most_occurrent_nodes:
        node_dataframe = org_dataframe.loc[(org_dataframe[KEY] == node)]
        most_occurrent_nodes_dataframe.append(node_dataframe)
        collected_num += 1
        if collected_num > THRESHOLD:
            break

    # for df in most_occurrent_nodes_dataframe:
    #     print(f'DataFrame:\n{df}')

    if os.path.exists(FILE_MOST_OCCUR_NODES):
        os.remove(FILE_MOST_OCCUR_NODES)

    for node_dataframe in most_occurrent_nodes_dataframe:
        node_dataframe.to_csv(FILE_MOST_OCCUR_NODES, mode='a')

    return most_occurrent_nodes_dataframe


def filter_attributes(headers, data):
    # Filter attributes based on dataframes of grouped nodes
    nodes_dataframes = get_dataframe_CompId(headers, data)
    filtered_attributes = []

    if os.path.exists(FILE_FILTERED_ATTRIBUTES):
        os.remove(FILE_FILTERED_ATTRIBUTES)

    indicator = 0
    for node_dataframe in nodes_dataframes:
        attributes_dataframe = node_dataframe[ATTRIBUTE_SERVICE_TIME]
        if indicator == 0:
            print(f'ATTTRIBUTE DATAFRAME:\n{attributes_dataframe}')
            indicator += 1
        filtered_attributes.append(attributes_dataframe)

        attributes_dataframe.to_csv(FILE_FILTERED_ATTRIBUTES, mode='a')

    return filtered_attributes


# if '__main__' == __name__:
#     parent_file_path = r'/home/huzq85/courses/626-simulation/dataset'
#     header_file = r'/home/huzq85/courses/626-simulation/hw2/sample_dataset/HEADER'
#     # "20160815": 4.7 GB; "20170328":1.98 GB; "20180711"||"20180711-fake": Small size
#     data_file = os.path.join(parent_file_path, '20160815')
#     headers = get_header(header_file)
#     data = get_data(data_file)
#     filter_attributes(headers, data)
