import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
from helper import *


def load_data(file_name):
    data = ''
    with open(file_name, 'r') as f:
        data = f.readline()

    # Do we need to include the last element?
    data_list = data.split(' ')[1:-1]
    print(data_list)
    print(len(data_list))
    data_list = [float(x) for x in data_list if x != '']
    return data_list


def compute_CDF(data):
    data = sorted(data)
    data_len = len(data)
    prev = data[0]
    support = [prev]
    ECDF = [0.]
    for cur in range(1, data_len):
        data_cur = data[cur]
        if data_cur != prev:
            preP = cur/data_len
            support.append(prev)
            ECDF.append(preP)
            support.append(data_cur)
            ECDF.append(preP)
            prev = data_cur
    support.append(prev)
    ECDF.append(1.)
    return support, ECDF


def compute_PDF(data, bin_size):
    # count, bins_count = np.histogram(data, bins=bin_size, range=(0, 0.1))
    count, bins_count = np.histogram(data, bins=bin_size)
    pdf = count / sum(count)
    return bins_count[1:], pdf


def plot_PDF_CDF(data, file_name):
    # The ideal bin size is approximately equal to floor(5/3 * n**(1/3))

    # Plotting CDF
    support, ECDF = compute_CDF(data)
    plt.plot(support, ECDF, color='blue', label='CDF')
    plt.legend()
    plt.savefig(f'CDF_{file_name}.jpg')
    plt.clf()
    # Plotting PDF

    data_len = len(data)
    ideal_bin_size = (5/3) * (data_len ** (1/3))
    bins = [50, 100, 150, 200, math.floor(ideal_bin_size)]

    for bin in bins:
        bins_count, pdf = compute_PDF(data, bin)
        plt.plot(bins_count, pdf, color="red", label="PDF")
        plt.legend()
        plt.savefig(f'PDF_{file_name}_{bin}.jpg')
        plt.clf()


def plot_Autocorrelation(data, file_name):
    # x = pd.plotting.autocorrelation_plot(data)
    # x.plot()
    # plt.xlabel("Lags")
    # plt.acorr(data)
    # plt.grid(True)
    # plt.savefig(f'{file_name}.jpg')
    # plt.clf()
    plot_acf(data, lags=100)
    plt.xlabel("Lags")
    plt.grid(True)
    plt.savefig(f'{file_name}.jpg')
    plt.clf()


if '__main__' == __name__:

    data_files = get_data_set_file(os.getcwd(), '.txt')

    # print(data_files)
    for data_file in data_files:
        filename_elem = data_file.split('.')[0].split('_')
        if 'inter_arrival_times' in data_file:
            data_inter_arrival = load_data(data_file)
            plot_PDF_CDF(data_inter_arrival,
                         f'inter-arrival-{filename_elem[-2]}-{filename_elem[-1]}')

            plot_Autocorrelation(
                data_inter_arrival, f'autocor-inter-arrival-{filename_elem[-2]}-{filename_elem[-1]}')

        if 'service_times' in data_file:
            data_service_time = load_data(data_file)
            plot_PDF_CDF(
                data_service_time, f'service-time-{filename_elem[-2]}-{filename_elem[-1]}')

            plot_Autocorrelation(
                data_service_time, f'autocor-service-time-{filename_elem[-2]}-{filename_elem[-1]}')
