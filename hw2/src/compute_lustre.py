import numpy as np
from preprocessing_lustre import *

TIMESTAMP = 'time'
MEASUREMENT_TIME = 'measurement_time'

SAVED_DATA_FILE = 'max_data_frame'


def normalize(data):
    # print(f'min value: {np.min(data)}')
    # print(f'max value: {np.max(data)}')
    new_list = (data-np.min(data))/(np.max(data)-np.min(data))
    return list(new_list)


def compute_inter_arrival(dataframe):
    timestamps = dataframe[TIMESTAMP]

    print(f'Timestamps:\n{timestamps}')
    inter_arrival_times = []

    timestamps_list = timestamps.to_list()

    for curIndex in range(len(timestamps_list)-1):
        inter_arrival = float(
            timestamps_list[curIndex+1]) - float(timestamps_list[curIndex])
        inter_arrival_times.append(inter_arrival)

    print(f'Inter-arrival Times:\n{inter_arrival_times}')

    normalized_inter_arrival_times = normalize(inter_arrival_times)
    mean, variance, std = compute_basic_statistics(
        normalized_inter_arrival_times)
    cv = std/mean
    print('Statistics of Inter-arrival Times:')
    print(f'Mean: {mean:.6f}')
    print(f'variance: {variance:.6f}')
    print(f'Standard Deviation: {std:.6f}')
    print(f'C.V.: {cv:.6f}')
    return inter_arrival_times, normalized_inter_arrival_times


def get_arrival_times(dataframe):
    arrival_times = dataframe[TIMESTAMP]
    print(f'Arrival Time:\n{arrival_times}')

    arrival_times = [float(a_time) for a_time in arrival_times]

    return arrival_times


def get_service_time(dataframe):
    service_times = dataframe[MEASUREMENT_TIME]
    print(f'Service Time:\n{service_times}')

    service_times = [float(s_time) for s_time in service_times]
    normalized_service_times = normalize(service_times)
    # print(f'Normalized Service Times:\n{normalized_service_times}')
    mean, variance, std = compute_basic_statistics(normalized_service_times)
    cv = std/mean

    print('Statistics of Service Times:')
    print(f'Mean: {mean:.6f}')
    print(f'variance: {variance:.6f}')
    print(f'Standard Deviation: {std:.6f}')
    print(f'C.V.: {cv:.6f}')
    return service_times, normalized_service_times


def compute_basic_statistics(list_vals):
    # Return the mean, variance and standard deviations
    return np.mean(list_vals), np.var(list_vals), np.std(list_vals)


def saving_compute_results(file_name, data):
    with open(file_name, 'w') as f:
        for elem in data:
            f.write(str(elem) + ' ')


def get_data_set_file(file_folder):
    files = os.listdir(file_folder)
    data_files = []
    for f in files:
        if f.endswith('csv') and SAVED_DATA_FILE in f:
            data_files.append(f)
    return data_files


if '__main__' == __name__:
    data_files = get_data_set_file(os.getcwd())

    for data_file in data_files:
        data_frame = load_data_frame(data_file)
        inter_arrivals, inter_arrival_normalized = compute_inter_arrival(
            data_frame)
        service_times, service_times_normalized = get_service_time(data_frame)

        arrival_times = get_arrival_times(data_frame)

        filename_elem = data_file.split('.')[0].split('_')

        saving_compute_results(
            f'inter_arrival_times_normalized_{filename_elem[-2]}_{filename_elem[-1]}.txt', inter_arrival_normalized)

        saving_compute_results(
            f'inter_arrival_times_{filename_elem[-2]}_{filename_elem[-1]}.txt', inter_arrivals)

        saving_compute_results(
            f'service_times_normalized_{filename_elem[-2]}_{filename_elem[-1]}.txt', service_times_normalized)

        saving_compute_results(
            f'service_times_{filename_elem[-2]}_{filename_elem[-1]}.txt', service_times)

        saving_compute_results(
            f'arrival_times_{filename_elem[-2]}_{filename_elem[-1]}.txt', arrival_times)
