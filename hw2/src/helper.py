import os

SAVED_DATA_FILE = 'max_data_frame'


def get_data_set_file(file_folder, file_type):
    files = os.listdir(file_folder)
    data_files = []
    for f in files:
        if (file_type == '.csv' and SAVED_DATA_FILE in f) or (file_type == '.txt' and os.path.splitext(f)[1] == '.txt'):
            data_files.append(f)

    return data_files
