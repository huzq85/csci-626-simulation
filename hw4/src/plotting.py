from matplotlib import markers
import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline
# from helper import *


def load_data(file_name):
    data = []

    lag_indices = []
    acf_vals = []
    with open(file_name, 'r') as f:
        data = f.readlines()

    for data_str in data:
        [lag_index, acf_val] = data_str.split(' ')

        lag_indices.append(int(lag_index))
        acf_vals.append(float(acf_val.strip()))

    return lag_indices, acf_vals


def plot_Autocorrelation(file1, file2, file3):
    lag_size, acf1 = load_data(file1)
    acf_vals_norm1 = np.linalg.norm(acf1)

    lag_size, acf2 = load_data(file2)
    acf_vals_norm2 = np.linalg.norm(acf2)

    lag_size, acf3 = load_data(file3)
    acf_vals_norm3 = np.linalg.norm(acf3)

    # lag_size, acf4 = load_data(file4)
    # acf_vals_norm4 = np.linalg.norm(acf4)

    y1 = acf1
    y2 = acf2
    y3 = acf3
    # y4 = acf4

    x = [elem for elem in range(10)]

    plt.ylim(0, 1)
    plt.xlabel('Lag')
    plt.ylabel('Autocorrelation')
    # plt.title('')
    # plt.plot(lag_indices, acf_vals)

    plt.plot(lag_size, y1, 'r', label='batch_size=10')
    plt.plot(lag_size, y2, 'g', label='batch_size=20')
    plt.plot(lag_size, y3, 'b', label='batch_size=50')
    plt.legend()
    # plt.show()
    plt.savefig('ACF_Batches.jpg')


def plotting_intervals(trace_file='', simu_file=''):
    x = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    y = [0.1035,
         0.2037,
         0.3032,
         0.4029,
         0.5023,
         0.6016,
         0.7009,
         0.7996,
         0.8979

         ]
    intervals = [0.0078,
                 0.0057,
                 0.0051,
                 0.0050,                0.0046,
                 0.0042,
                 0.0036,                0.0024,
                 0.0012

                 ]
    y_error = []

    # y_lower = []
    # y_upper = []

    for index in range(len(y)):
        if y[index] != 0:
            y_error.append(intervals[index]/y[index])
        else:
            y_error.append(0)
        # y_lower.append(y[index] - intervals[index])
        # y_upper.append(y[index] + intervals[index])

    plt.scatter(x, y, c='red')
    # plt.plot(x, y)
    # plt.plot(x, y_lower)
    # plt.plot(x, y_upper)

    plt.errorbar(x, y, yerr=intervals, color='green')

    plt.xlabel('Utilization')
    plt.ylabel('Estimation')

    # # Plot the confidence interval
    # plt.fill_between(x, (y-ci), (y+ci), color='blue', alpha=0.1)
    plt.savefig('confidence_inerval.jpg')


if '__main__' == __name__:
    # acf_data = r'/home/zhu05/course/626-simulation/hw3/src/acf_trace_lag_100.dat'
    # plot_Autocorrelation('acf_data_lag_10_10.dat',
    #                      'acf_data_lag_10_20.dat',
    #                      'acf_data_lag_10_50.dat'
    #                      )
    plotting_intervals()
    # plotting(r'trace_mean_delay.dat', 'simu_mean_delay.dat')
