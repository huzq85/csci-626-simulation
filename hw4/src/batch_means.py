# ----------------------------------------------------------------------
# * This program reads a data sample from a text file in the format
# *                         one data point per line
# * and calculates an interval estimate for the mean of that (unknown) much
# * larger set of data from which this sample was drawn.  The data can be
# * either discrete or continuous.  A compiled version of this program
# * supports redirection and can used just like program uvs.c.
# *
# * Name              : estimate.c  (Interval Estimation)
# * Author            : Steve Park & Dave Geyer
# * Language          : ANSI C
# * Latest Revision   : 11-16-98
#  # Translated by     : Philip Steele
#  # Language          : Python 3.3
#  # Latest Revision   : 3/26/14
# * ----------------------------------------------------------------------
# */

from rvms import idfStudent
from math import sqrt
import sys
import argparse


def compute(batch_size):
    LOC = 0.95                             # level of confidence,        */
    # use 0.95 for 95% confidence */

    n = 0                     # counts data points */
    sum_val = 0.0
    mean = 0.0
    # double data
    # double stdev
    # double u, t, w
    # double diff

    total_data = []

    batch_mean_arr = []
    cur_index = 0
    sub_index = 0

    data = sys.stdin.readline()
    while(data):
        total_data.append(float(data))
        data = sys.stdin.readline()

    count = 0
    while(cur_index < len(total_data)):
        count += 1
        sub_sum = 0.0
        while(sub_index < batch_size):
            sub_sum += total_data[cur_index]
            sub_index += 1
            cur_index += 1
        batch_mean_arr.append(sub_sum/batch_size)
        sub_index = 0

        if (len(total_data) - cur_index < batch_size):
            break

    print(
        f'Total count of data points is {len(total_data)}. {len(total_data) - cur_index} data points have been discarded as batch size is {batch_size}.')
    print(f'Mean value is: {sum(total_data)/len(total_data)}')
    print(f'(b,k) = {batch_size, int(len(total_data)/batch_size)}')

    with open(f'batch_means_arr_{batch_size}.dat', 'w') as f:
        for data in batch_mean_arr:
            f.write(str(data) + '\n')

    for data in batch_mean_arr:                         # use Welford's one-pass method */
        n += 1                              # to calculate the sample mean  */
        diff = float(data) - mean          # and standard deviation        */
        sum_val += diff * diff * (n - 1.0) / n
        mean += diff / n
        # data = sys.stdin.readline()
    # EndWhile
    stdev = sqrt(sum_val / n)

    if (n > 1):
        u = 1.0 - 0.5 * (1.0 - LOC)              # interval parameter  */
        t = idfStudent(n - 1, u)                 # critical value of t */
        w = t * stdev / sqrt(n - 1)              # interval half width */
        print("based upon {0:1d} data points and with {1:d} confidence".format(
            n, int(100.0 * LOC + 0.5)))
        print(
            "the expected value is in the interval {0:10.4f} +/- {1:6.4f}".format(mean, w))

    else:
        print("ERROR - insufficient data\n")

    # C output:
    # bash.exe"-3.1$ ./estimate.exe < uvs.dat

    # based upon 1000 data points and with 95% confidence
    # the expected value is in the interval      3.04 +/-   0.11


if '__main__' == __name__:
    parser = argparse.ArgumentParser(
        description='Add a parameter to specify the size of the batch')
    parser.add_argument('-b', '--bsize',  type=int, required=True)

    args = parser.parse_args()
    b_size = args.bsize
    compute(b_size)
