

def write_file(file_name, contents):
    with open(file_name, 'w', encoding='utf-8', newline='') as f:
        contents_str = [str(content) + ' ' for content in contents]
        f.writelines(contents_str)
