import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from sklearn import preprocessing
from helper import *


def load_data(file_name):
    data = []

    lag_indices = []
    acf_vals = []
    with open(file_name, 'r') as f:
        data = f.readlines()

    for data_str in data:
        [lag_index, acf_val] = data_str.split(' ')

        lag_indices.append(int(lag_index))
        acf_vals.append(float(acf_val.strip()))

    return lag_indices, acf_vals


def plot_Autocorrelation(file1, file2, file3):
    lag_size, acf1 = load_data(file1)
    acf_vals_norm1 = np.linalg.norm(acf1)

    lag_size, acf2 = load_data(file2)
    acf_vals_norm2 = np.linalg.norm(acf2)

    lag_size, acf3 = load_data(file3)
    acf_vals_norm3 = np.linalg.norm(acf3)

    y1 = acf1/acf_vals_norm1
    y2 = acf2/acf_vals_norm2
    y3 = acf3/acf_vals_norm3

    # y1 = acf1
    # y2 = acf2
    # y3 = acf3

    x = [elem+50 for elem in range(200)]

    plt.ylim(-0.2, 0.3)  # Simulation
    # plt.ylim(0, 1)  # Trace
    plt.xlabel('Lag')
    plt.ylabel('Autocorrelation')
    # plt.title('')
    # plt.plot(lag_indices, acf_vals)

    plt.plot(lag_size, y1, 'r', label='util=0.2')
    plt.plot(lag_size, y2, 'g', label='util=0.5')
    plt.plot(lag_size, y3, 'b', label='util=0.9')
    plt.legend()
    # plt.show()
    plt.savefig('ACF_Simulate_inter_arrival_cmp.jpg')


def plotting(trace_file, simu_file):
    x = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    # x = [0.1, 0.2, 0.3, 0.4, 0.6, 0.8]

    y1 = []
    y2 = []

    with open(trace_file, 'r') as f:
        y1 = f.readlines()

    with open(simu_file, 'r') as f:
        y2 = f.readlines()

    y1 = [float(elem.strip()) for elem in y1]
    y2 = [float(elem.strip()) for elem in y2]

    y1 = np.array(y1).astype(np.double)
    y2 = np.array(y2).astype(np.double)

    y1norm = np.linalg.norm(y1)
    y2norm = np.linalg.norm(y2)

    y1 = y1/y1norm
    y2 = y2/y2norm

# y2 = [elem for elem in y2 if elem != 0]
# print(y2)

    # X_Y_Spline = make_interp_spline(x, y2)
    # Y_ = X_Y_Spline(x)
    plt.xlabel('Utilization')
    plt.ylabel('Mean Delay Times')
    plt.plot(x, y1, 'r', label='trace')
    plt.plot(x, y2, 'b', label='simulation')
    plt.legend()

    plt.savefig('mean_delay.jpg')


if '__main__' == __name__:
    # acf_data = r'/home/zhu05/course/626-simulation/hw3/src/acf_trace_lag_100.dat'
    plot_Autocorrelation('acf_data_simulate_0.19.dat',
                         'acf_data_simulate_0.49.dat', 'acf_data_simulate_0.87.dat')
    # plotting(r'trace_mean_delay.dat', 'simu_mean_delay.dat')
