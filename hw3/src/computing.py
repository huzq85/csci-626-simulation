import os
from turtle import delay
import pandas as pd
from helper import *


def read_file(file):
    elems = []
    with open(file, 'r', encoding='utf-8', newline='') as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
        for line in lines:
            tmp_elems = line.split(' ')
        elems.append(tmp_elems)
    elems = [item for sub_list in elems for item in sub_list]
    return elems


def compute_comp_delay_times(arrival_times, service_times):
    # Delay time for every job can be computed based on Algorithm 1.2.1

    complete_times = []
    assert(len(arrival_times) == len(service_times))

    delay_times = []
    complete_times = []

    # Compute delay time for every job in the following

    pre_c = 0.0
    cur_d = 0.0

    for index in range(len(arrival_times)):
        cur_a = arrival_times[index]
        if cur_a < pre_c:
            cur_d = pre_c - cur_a
        else:
            cur_d = 0.0
        cur_s = service_times[index]
        cur_c = cur_a + cur_d + cur_s
        delay_times.append(cur_d)
        complete_times.append(cur_c)
        pre_c = cur_c
    return complete_times, delay_times


def compute_mean_wait_time(service_times, delay_times):
    wait_times = []
    for idx in range(len(service_times)):
        wait_times.append(service_times[idx] + delay_times[idx])
    return sum(wait_times)/len(wait_times)


def consistency_checking(delay_times, service_times):
    mean_delay = sum(delay_times)/len(delay_times)
    mean_service = sum(service_times)/len(service_times)
    print(f'mean delay: {mean_delay}, mean service: {mean_service}')


def compute_system_utilization(service_times, complete_times):
    return sum(service_times)/complete_times[-1]


def test_util(arrival_times, delay_times, service_times):
    return sum(service_times)/(arrival_times[-1] + delay_times[-1] + service_times[-1])


def test_util2(arrival_times, service_times):
    return sum(service_times)/len(service_times)/arrival_times[-1] * len(arrival_times)


def compute_waiting_que_length():
    pass


def compute_departure_autocorrelation():
    pass


def compute():
    pass


if '__main__' == __name__:
    ia_file = r'/home/zhu05/course/626-simulation/hw3/src/arrival_times_scratch_201508.txt'
    st_file = r'/home/zhu05/course/626-simulation/hw3/src/service_times_scratch_201508.txt'
    # ia_file = r'/home/zhu05/course/626-simulation/hw3/src/debug_arr_times.txt'
    # st_file = r'/home/zhu05/course/626-simulation/hw3/src/debug_service_times.txt'
    arrival_times = read_file(ia_file)
    service_times = read_file(st_file)

    arrival_times = [float(a_time) for a_time in arrival_times]
    service_times = [float(s_time) for s_time in service_times]

    complete_times, delay_times = compute_comp_delay_times(
        arrival_times, service_times)

    consistency_checking(delay_times, service_times)
    mean_wait_time = compute_mean_wait_time(service_times, delay_times)
    print(f'Mean wait time: {mean_wait_time}')

    utilzation = compute_system_utilization(service_times, complete_times)
    # util = test_util(arrival_times, delay_times, service_times)
    # util2 = test_util2(arrival_times, service_times)
    print(f'System Utilzation: {utilzation}')

    # print(f'Complete Times:\n{complete_times}')
    # print(f'Delay Times:\n{delay_times}')

    # write_file('Complete_Times.csv', complete_times)
    # write_file('Delay_Times.csv', delay_times)

    # compute()
