
# -------------------------------------------------------------------------
# * This program - an extension of program ssq1.c - simulates a single-server
# * FIFO service node using Exponentially distributed interarrival times and
# * Uniformly distributed service times (i.e. a M/U/1 queue).
# *
# * Name              : ssq2.c  (Single Server Queue, version 2)
# * Author            : Steve Park & Dave Geyer
# * Language          : ANSI C
# * Latest Revision   : 9-11-98
#   Translated by   : Philip Steele
#   Language        : Python 3.3
#   Latest Revision : 3/26/14
# * -------------------------------------------------------------------------
# */

from rng import putSeed, random
from math import log

LAST = 10000                   # number of jobs processed */
START = 0.0                      # initial time             */
arrivalTemp = START  # global for getArrival


def Exponential(m):
    # ---------------------------------------------------
    # * generate an Exponential random variate, use m > 0.0
    # * ---------------------------------------------------
    # */
    return (-m * log(1.0 - random()))


def Uniform(a, b):
    # --------------------------------------------
    # * generate a Uniform random variate, use a < b
    # * --------------------------------------------
    # */
    return (a + (b - a) * random())


def GetArrival():
    # ------------------------------
    # * generate the next arrival time
    # * ------------------------------
    # */
    global arrivalTemp

    arrivalTemp += Exponential(2.0)
    return (arrivalTemp)


def GetService():
    # ------------------------------
    # * generate the next service time
    # * ------------------------------
    # */
    return (Uniform(1.0, 2.0))


class sumOf:
    delay = 0.0  # delay times
    wait = 0.0  # wait times
    service = 0.0  # service times
    interarrival = -1.0  # interarrival times


#################################Main Program############################
mean_waiting_queue_length = [0, 0, 0, 0, 0, 0, 0, 0, 0]
mean_delay = [0, 0, 0, 0, 0, 0, 0, 0, 0]

mask_arr = [False, False, False, False, False, False, False, False, False]
last_util = 0

for k in range(1, 1000, 1):

    index = 0                        # job index            */
    arrival = START                    # arrival time         */
    delay = -1                               # delay in queue       */
    service = -1                             # service time         */
    wait = -1                                 # delay + service      */
    departure = START                    # departure time       */
    sum = sumOf()

    putSeed(123456789)

    # read in arrivals and service times from file
    try:
        fp = open('cvt_format_arrival_service.dat', 'r')
    except IOError:
        print("File not found")
        exit()

    arrivals = []
    services = []

    departures = ''

    for line in fp:
        arrivals.append(float(line.split()[0])-1438405203.0)
        services.append(float(line.split()[1]))

    # while (index < LAST):

    arrival_times = []
    departure_times = []
    service_times = []

    group_queue_len = []
    group_delay = []
    for i in range(0, len(arrivals), k):
        # print(f'DEBUG: {i}')
        index += 1
        # arrival      = GetArrival()
        arrival = arrivals[i]
        arrival_times.append(arrival)
        if (arrival < departure):
            delay = departure - arrival         # delay in queue    */
        else:
            delay = 0.0                         # no delay          */
        # service      = GetService()
        service = services[i]
        wait = delay + service
        departure = arrival + wait              # time of departure */
        departures += str(departure) + ' '
        departure_times.append(departure)
        sum.delay += delay
        sum.wait += wait
        sum.service += service
        service_times.append(service)
    # EndWhile
    sum.interarrival = arrival - START

    utilization = float('{:.2f}'.format(sum.service / departure))
    if (utilization < 1.0 and utilization >= 0.1):
        # utilization = (sum.service / departure)
        mask_arr_index = int(utilization/0.1)-1

        if (mask_arr_index < len(mask_arr) and mask_arr[mask_arr_index] == False):
            mask_arr[mask_arr_index] = True

            print("for {0} jobs".format(index))
            print("   average interarrival time = {0:6.2f}".format(
                sum.interarrival / index))  # This should be correct
            print("   average wait ............ = {0:6.2f}".format(
                sum.wait / index))

            print("   average delay ........... = {0:6.2f}".format(
                sum.delay / index))

            print("   average service time .... = {0:6.2f}".format(
                sum.service / index))
            print("   average # in the node ... = {0:6.2f}".format(
                sum.wait / departure))
            print("   average # in the queue .. = {0:6.2f}".format(
                sum.delay / departure))

            print("   utilization ............. = {0:6.2f}".format(
                sum.service / departure))
            print(f'   an = {arrival_times[-1]}, cn = {departure_times[-1]}')

            mean_waiting_queue_length[mask_arr_index] = (sum.delay / departure)
            mean_delay[mask_arr_index] = (sum.delay / index)
            # else:
            # last_util = utilization
            # mean_waiting_queue_length.append(
            #     sum(group_queue_len)/len(group_queue_len))
            # mean_delay.append(sum(group_delay)/len(group_delay))

            inter_departures = []
            for cur_index in range(len(departure_times)):
                if (cur_index+1) < len(departure_times):
                    inter_depart = departure_times[cur_index +
                                                   1] - departure_times[cur_index]
                    inter_departures.append(inter_depart)

            inter_arrivals = []
            for cur_index in range(len(arrival_times)):
                if (cur_index+1) < len(arrival_times):
                    inter_arrival = arrival_times[cur_index +
                                                  1] - arrival_times[cur_index]
                    inter_arrivals.append(inter_arrival)

            with open(f'inter_departure_trace_{utilization}.dat', 'w') as f:
                for elem in inter_departures:
                    f.write(str(elem) + '\n')

            with open(f'service_time_trace_{utilization}.dat', 'w') as f:
                for elem in service_times:
                    f.write(str(elem) + '\n')

            with open(f'inter_arrival_trace_{utilization}.dat', 'w') as f:
                for elem in inter_arrivals:
                    f.write(str(elem) + '\n')

            if False in mask_arr:
                continue
            else:
                # mean_waiting_queue_length.sort()
                # mean_delay.sort()
                break


with open(f'trace_mean_delay.dat', 'w') as f:
    for elem in mean_delay:
        f.write(str(elem) + '\n')

with open(f'trace_mean_queue_length.dat', 'w') as f:
    for elem in mean_waiting_queue_length:
        f.write(str(elem) + '\n')

# C output:
# for 10000 jobs
#    average interarrival time =   2.02
#    average wait ............ =   3.86
#    average delay ........... =   2.36
#    average service time .... =   1.50
#    average # in the node ... =   1.91
#    average # in the queue .. =   1.17
#    utilization ............. =   0.74
