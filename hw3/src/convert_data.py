import os


def read_data(read_file):
    elems = []
    with open(read_file, 'r', encoding='utf-8', newline='') as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
        for line in lines:
            tmp_elems = line.split(' ')
        elems.append(tmp_elems)
    elems = [item for sub_list in elems for item in sub_list]
    return elems


def write_data(arrival_times, service_times):
    if (len(arrival_times) != len(service_times)):
        print(f'Arrival times are inconsistency with Service times!')
        return
    total_lines = []

    for idx in range(len(arrival_times)):
        total_lines.append(
            arrival_times[idx] + ' ' + service_times[idx] + '\n')

    with open('converted_format_arrival_service.dat', 'w') as f:
        f.writelines(total_lines)


def convert_departure_data(file_name):
    elems = read_data(file_name)

    with open(f'cvt_inter_departure_data.dat', 'w') as f:
        for elem in elems:
            f.write(elem + '\n')
    print('Convert Data Done!')


if '__main__' == __name__:
    # ia_file = r'/home/zhu05/course/626-simulation/hw3/src/arrival_times_scratch_201508.txt'
    # st_file = r'/home/zhu05/course/626-simulation/hw3/src/service_times_scratch_201508.txt'
    # ia_file = r'D:\626-simulation\hw3\resources\arrival_times_scratch_201508.txt'
    # st_file = r'D:\626-simulation\hw3\resources\service_times_scratch_201508.txt'
    # arrival_times = read_data(ia_file)
    # service_times = read_data(st_file)
    # write_data(arrival_times, service_times)
    convert_departure_data(
        r'/home/zhu05/course/626-simulation/hw3/src/inter_departure.dat')
